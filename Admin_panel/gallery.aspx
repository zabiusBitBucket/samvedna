﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gallery.aspx.cs" Inherits="gallery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Samvedna Admin Panel</title>  

    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet"/>
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <!--//webfonts-->
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="js/wow.min.js"></script>
  
    <!-- Metis Menu -->
    <script src="js/metisMenu.min.js"></script>
    <script src="js/custom.js"></script>
    <link href="css/custom.css" rel="stylesheet"/>
    <!--//Metis Menu -->

    <link href="css/index.css" rel="stylesheet"/>
    <script src="js/bootstrap-wysiwyg.js"></script>

    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet"/>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet"/>
    <script src="js/jquery.hotkeys.js"></script>


</head>
<body class="cbp-spmenu-push">
    <form runat="server" id="form1">
    <div class="main-content">
        <!--left-fixed -navigation-->
        <div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                    <ul class="nav" id="side-menu">                        
                        <li>
                            <a href="events.aspx" class="active"><i class="fa fa-cogs nav_icon"></i>Events</a>

                        </li>
                        <li class="">
                            <a href="projects.aspx"><i class="fa fa-book nav_icon"></i>Projects</a>

                        </li>
                        <li>
                            <a href="gallery.aspx" class="active"><i class="fa fa-th-large nav_icon"></i>Gallery</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-envelope nav_icon"></i>Testimonials</a>

                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table nav_icon"></i>Send SMS</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check-square-o nav_icon"></i>Track Payments</a>

                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text-o nav_icon"></i>Trustees</a>

                        </li>
                        <li>
                            <a href="charts.html" class="chart-nav"><i class="fa fa-bar-chart nav_icon"></i>Contact Form</a>
                        </li>
                    </ul>
                    <div class="clearfix"> </div>
                    <!-- //sidebar-collapse -->
                </nav>
            </div>
        </div>
        <!--left-fixed -navigation-->
        <!-- header-starts -->
        <div class="sticky-header header-section ">
            <div class="header-left">
                <!--toggle button start-->
                <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                <!--toggle button end-->
                <!--logo -->
                <div class="logo">
                    <a href="index.html">
                        <h1>SAMVEDNA</h1>
                        <span>AdminPanel</span>
                    </a>
                </div>
                <!--//logo-->

                <div class="clearfix"> </div>
            </div>
            <div class="header-right">

                <div class="profile_details">
                    <ul>
                        <li class="dropdown profile_details_drop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile_img">
                                    <span class="prfil-img"><img src="images/a.png" alt=""> </span>
                                    <div class="user-name">
                                        <p>Wikolia</p>
                                        <span>Administrator</span>
                                    </div>
                                    <i class="fa fa-angle-down lnr"></i>
                                    <i class="fa fa-angle-up lnr"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                            <ul class="dropdown-menu drp-mnu">

                                <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- //header-ends -->
        <!-- main content start-->
        <div id="page-wrapper">
             <div class="main-page signup-page">
                <h3 class="title1">Upload Media</h3>
                <div class="sign-up-row widget-shadow">
                    <h5>Project Details :</h5>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Caption* :</h4>
                        </div>
                        <div class="sign-up2">
                            <asp:TextBox runat="server" ID="txtCaption" required="true"></asp:TextBox>
                           <%-- <input type="text" required>--%>

                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Category* :</h4>
                        </div>
                        <div class="sign-up2">
                            <asp:TextBox runat="server" ID="txtCategory" required="true"></asp:TextBox>
                            <%--<input type="text" required>--%>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Image :</h4>
                        </div>
                        <div class="sign-up2">
                            <asp:FileUpload runat="server" ID="fileUploader" type="file" value="Browse" style="margin-top:1.2em; margin-left:1em;" />
                            <%--<input type="file" value="Browse" style="margin-top:1.2em; margin-left:1em;" />--%>
                        </div>

                        <div class="clearfix"> </div>
                    </div>




                    <div class="sub_home">
                        <asp:Button runat="server" ID="btnSumbit" type="submit" value="Submit" class="pull-right" Text="Submit" />
                       <%-- <input type="submit" value="Submit" class="pull-right">--%>

                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>

            <div class="row signup-page">
                <div class="col-md-2"></div>
                <div class="col-md-10 sign-up-row widget-shadow">
                    <h5>Recently added projects</h5>

                    <div class="panel-body ">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Caption</th>
                                    <th>Category</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody style="vertical-align:middle;">
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Charity for education</td>
                                    <td>Child</td>
                                    <td class="col-md-2"><span><img src="images/1.jpg" style="max-width:90%;" /></span></td>
                                    <td><a href="#"><span class="fa fa-times"></span> Delete</a> </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Charity for education</td>
                                    <td>Child</td>
                                    <td class="col-md-2"><span><img src="images/2.png" style="max-width:90%;" /></span></td>
                                    <td><a href="#"><span class="fa fa-times"></span> Delete</a> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!--footer-->
        <div class="footer">
            <p>&copy; 2016 Samvedna Admin Panel. All Rights Reserved | Design by <a href="https://zabius.com/" target="_blank">Zabius Technologies</a></p>
        </div>
        <!--//footer-->
    </div>
        </form>
    <!-- Classie -->
    <script src="js/classie.js"></script>
    <script>
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
            showLeftPush = document.getElementById('showLeftPush'),
            body = document.body;

        showLeftPush.onclick = function () {
            classie.toggle(this, 'active');
            classie.toggle(body, 'cbp-spmenu-push-toright');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeftPush');
        };

        function disableOther(button) {
            if (button !== 'showLeftPush') {
                classie.toggle(showLeftPush, 'disabled');
            }
        }
    </script>
    <!--scrolling js-->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="js/scripts.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
</body>
</html>
