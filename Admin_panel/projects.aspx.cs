﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class projects : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
      //  var value = chkHomePage.Checked.ToString();
    }
    protected void Upload_Clicked(object sender, EventArgs e)
    {
        if (fileHeaderImage.PostedFile.ContentLength > 0)
        {
           // var value = chkHomePage.Checked.ToString();
            fileHeaderImage.SaveAs(Server.MapPath("~/images/" + fileHeaderImage.PostedFile.FileName));
            hdnimagepath.Value = fileHeaderImage.PostedFile.FileName;
            
        }

    }
    [WebMethod]

    public static void Insert_Project(string Imgname, string txtProjectTitle, string txtRaiseAmount, string Descrption,string isHomePage)
    {
        string DefaultImage = "d.jpg";

        SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlCommand cmd1 = new SqlCommand("insert_Project", conn1);
        cmd1.CommandType = CommandType.StoredProcedure;
        cmd1.Parameters.AddWithValue("@img", String.IsNullOrEmpty(Imgname) ? DefaultImage : Imgname);
        cmd1.Parameters.AddWithValue("@Title", txtProjectTitle);
        cmd1.Parameters.AddWithValue("@RaiseAmount",Convert.ToDecimal(txtRaiseAmount));
        cmd1.Parameters.AddWithValue("@Description", Descrption);
        cmd1.Parameters.AddWithValue("@HomePageImage",isHomePage=="true"?1:0);
      
        conn1.Open();
        cmd1.ExecuteNonQuery();
        conn1.Close();

    }
}