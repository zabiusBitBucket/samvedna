﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="loginNew" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SAMVEDNA Admin Panel</title>

    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet" />
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css' />
    <!--//webfonts-->
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/wow.min.js"></script>
    <%--	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->--%>
    <!-- Metis Menu -->
    <script src="js/metisMenu.min.js"></script>
    <script src="js/custom.js"></script>
    <link href="css/custom.css" rel="stylesheet" />
    <!--//Metis Menu -->
</head>
<body>
    <form id="form1" runat="server">
        <div id="page-wrapper">
            <div class="main-page login-page ">
                <h3 class="title1">SignIn Page</h3>
                <div class="widget-shadow">
                    <div class="login-top">
                        <h4>Welcome back to SAMVEDNA Admin Panel ! </h4>
                    </div>
                    <div class="login-body">
                        <asp:TextBox runat="server" ID="txtUserName" class="user" name="email" placeholder="Enter your email" required="true"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txtPassword" name="password" TextMode="Password"  class="lock" placeholder="password" required="true"></asp:TextBox>
                        <asp:Button runat="server" ID="btnLogin" name="Sign In" Text="Sign In" type="submit" OnClick="btnLogin_Click" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
