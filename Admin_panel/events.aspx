﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="events.aspx.cs" Inherits="eventsNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Samvedna Admin Panel</title>  

    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet"/>
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <!--//webfonts-->
    <!--animate-->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="js/wow.min.js"></script>
  
    <!-- Metis Menu -->
    <script src="js/metisMenu.min.js"></script>
    <script src="js/custom.js"></script>
    <link href="css/custom.css" rel="stylesheet"/>
    <!--//Metis Menu -->

    <link href="css/index.css" rel="stylesheet"/>
    <script src="js/bootstrap-wysiwyg.js"></script>

    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet"/>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet"/>
    <script src="js/jquery.hotkeys.js"></script>


</head>
<body class="cbp-spmenu-push">
   
    <form runat="server" id="form1">
    <div class="main-content">
        <!--left-fixed -navigation-->
        <div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                    <ul class="nav" id="side-menu">                        
                        <li>
                            <a href="events.aspx" class="active"><i class="fa fa-cogs nav_icon"></i>Events</a>

                        </li>
                        <li class="">
                            <a href="projects.aspx"><i class="fa fa-book nav_icon"></i>Projects</a>

                        </li>
                        <li>
                            <a href="widgets.html"><i class="fa fa-th-large nav_icon"></i>Gallery</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-envelope nav_icon"></i>Testimonials</a>

                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table nav_icon"></i>Send SMS</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-check-square-o nav_icon"></i>Track Payments</a>

                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text-o nav_icon"></i>Trustees</a>

                        </li>
                        <li>
                            <a href="charts.html" class="chart-nav"><i class="fa fa-bar-chart nav_icon"></i>Contact Form</a>
                        </li>
                    </ul>
                    <div class="clearfix"> </div>
                    <!-- //sidebar-collapse -->
                </nav>
            </div>
        </div>
        <!--left-fixed -navigation-->
        <!-- header-starts -->
        <div class="sticky-header header-section ">
            <div class="header-left">
                <!--toggle button start-->
                <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                <!--toggle button end-->
                <!--logo -->
                <div class="logo">
                    <a href="index.html">
                        <h1>SAMVEDNA</h1>
                        <span>AdminPanel</span>
                    </a>
                </div>
                <!--//logo-->

                <div class="clearfix"> </div>
            </div>
            <div class="header-right">

                <div class="profile_details">
                    <ul>
                        <li class="dropdown profile_details_drop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile_img">
                                    <span class="prfil-img"><img src="images/a.png" alt=""> </span>
                                    <div class="user-name">
                                        <p>Wikolia</p>
                                        <span>Administrator</span>
                                    </div>
                                    <i class="fa fa-angle-down lnr"></i>
                                    <i class="fa fa-angle-up lnr"></i>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                            <ul class="dropdown-menu drp-mnu">

                                <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- //header-ends -->
        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page signup-page">
                <h3 class="title1">Add an Event</h3>
                <div class="sign-up-row widget-shadow">
                    <h5>Event Details :</h5>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Title* :</h4>
                        </div>
                        <div class="sign-up2">

                            <asp:TextBox runat="server" ID="txtTitle" type="text" required="ture"></asp:TextBox>
                           

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Dated* :</h4>
                        </div>
                        <div class="sign-up2" style="padding:0px;">
                            <div class="col-md-4" style="padding:0px; margin-top: 4%;">
                                <div class='input-group date' id='datetimepicker1'>
                                    <asp:TextBox runat="server" ID="txtDate" type="text" required="ture" class="form-control" style="margin:0px;"></asp:TextBox>
                                    
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>

                            </div>
                            <div class="col-md-2" style="margin-top:5%;"><h4>Time :</h4></div>
                            <div class="col-md-6" style="padding:0px;">
                                <asp:TextBox runat="server" ID="txtStartTime" type="text" required="ture" placeholder="am" class="form-control" style="width:40%; margin-right:5px; float:left;"></asp:TextBox> 
                                <asp:TextBox runat="server" ID="txtEndTime" type="text" required="ture" placeholder="pm" class="form-control" style="width:40%;"></asp:TextBox>

                           
                            </div>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Location* :</h4>
                        </div>
                        <div class="sign-up2">
                            <asp:TextBox runat="server" ID="txtLocation" type="text" required="ture" class="form-control" ></asp:TextBox>                          

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Header Image :</h4>
                        </div>
                        <div class="sign-up2">

                            <asp:FileUpload runat="server" ID="fileHeaderImage" value="Browse" style="margin-top:1.2em; margin-left:1em;"  />
                            <asp:Button ID="Button1" OnClick="Button_Clicked" runat="server" Text="Upload" />
                            <asp:HiddenField ID="hdnimagepath" runat="server" />
                           
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="sign-u">
                        <div class="sign-up1">
                            <h4>Description :</h4>
                        </div>
                        <div class="sign-up2">

                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="row" style="margin:0;">
                        <div class="col-md-12">


                            <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="icon-font"></i><b class="caret"></b></a>
                                    <ul class="dropdown-menu"></ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                        <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                        <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                                    <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                                    <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
                                    <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
                                    <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
                                    <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                                    <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                                    <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                                    <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                                    <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>
                                    <div class="dropdown-menu input-append">
                                        <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                        <button class="btn" type="button">Add</button>
                                    </div>
                                    <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>
                                </div>

                                <div class="btn-group">
                                    <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                                    <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                                </div>
                                <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech=""/>
                            </div>
                            <div id="editor" runat="server">

                            </div>


                        </div>
                        <script>
        $(function () {
            function initToolbarBootstrapBindings() {
                var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                      'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                      'Times New Roman', 'Verdana'],
                      fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                $.each(fonts, function (idx, fontName) {
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                });
                $('a[title]').tooltip({ container: 'body' });
                $('.dropdown-menu input').click(function () { return false; })
                    .change(function () { $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); })
                .keydown('esc', function () { this.value = ''; $(this).change(); });

                $('[data-role=magic-overlay]').each(function () {
                    var overlay = $(this), target = $(overlay.data('target'));
                    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                });
                if ("onwebkitspeechchange" in document.createElement("input")) {
                    var editorOffset = $('#editor').offset();
                    $('#voiceBtn').css('position', 'absolute').offset({ top: editorOffset.top, left: editorOffset.left + $('#editor').innerWidth() - 35 });
                } else {
                    $('#voiceBtn').hide();
                }
            };
            function showErrorAlert(reason, detail) {
                var msg = '';
                if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
                else {
                    console.log("error uploading file", reason, detail);
                }
                $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                 '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
            };
            initToolbarBootstrapBindings();
            $('#editor').wysiwyg({ fileUploadError: showErrorAlert });
            window.prettyPrint && prettyPrint();
        });
                        </script>
                    </div>

                    

                    <div class="sub_home">

                          <input type="button" onclick="SaveEvent()" title="Submit" class="pull-right" value="Submit" />
                      <%--  <asp:Button runat="server" ID="btnServer" type="submit" value="Submit" class="pull-right" Text="Submit"  />--%>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>

            <div class="row signup-page">
                <div class="col-md-2"></div>
                <div class="col-md-10 sign-up-row widget-shadow">
                    <h5>Recently added events</h5>

                    <div class="panel-body ">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Dated</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Charity for education</td>
                                    <td>28/11/2016</td>
                                    <td>Ludhiana, Punjab</td>
                                    <td><a href="#"><span class="fa fa-times"></span> Delete</a> </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!--footer-->
        <div class="footer">
            <p>&copy; 2016 Samvedna Admin Panel. All Rights Reserved | Design by <a href="https://zabius.com/" target="_blank">Zabius Technologies</a></p>
        </div>
        <!--//footer-->
    </div>
        <script type="text/javascript" >

      function SaveEvent() {
          debugger;
          var def = "hello";
          var nameString = $("#hdnimagepath").val();

          $.ajax({
            type: "POST",
            url: "events.aspx/Insert_Event",         
            data: "{txtTitle:'" + $("#txtTitle").val() + "',txtEventTime:'" + $("#txtStartTime").val() + "-" + $("#txtEndTime").val() + "',txtLocation:'" + $("#txtLocation").val() + "',txtDate:'" + $("#txtDate").val() + "',Descrption:'" + $("#editor").html() + "',Imgname:'" + nameString + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: "true",
            success: function (response) {
                alert("done");
            },
            error: function (response) {
                alert(response.status + ' ' + response.statusText);
            }
            });        
      }
     </script>
        </form>
    <!-- Classie -->
    <script src="js/classie.js"></script>
    <script>
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
            showLeftPush = document.getElementById('showLeftPush'),
            body = document.body;

        showLeftPush.onclick = function () {
            classie.toggle(this, 'active');
            classie.toggle(body, 'cbp-spmenu-push-toright');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeftPush');
        };

        function disableOther(button) {
            if (button !== 'showLeftPush') {
                classie.toggle(showLeftPush, 'disabled');
            }
        }
    </script>
    <!--scrolling js-->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="js/scripts.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });
    </script>
     
</body>
</html>
