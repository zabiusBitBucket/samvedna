﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eventsNew : System.Web.UI.Page
{
      
    protected void Page_Load(object sender, EventArgs e)
    {
     

    }
    protected void Button_Clicked(object sender, EventArgs e)
    {
        if (fileHeaderImage.PostedFile.ContentLength > 0)
        {
            fileHeaderImage.SaveAs(Server.MapPath("~/images/" + fileHeaderImage.PostedFile.FileName));
            hdnimagepath.Value = fileHeaderImage.PostedFile.FileName;
        }

    }
    [WebMethod]
    
    public static void Insert_Event(string txtTitle, string txtEventTime, string txtLocation, string txtDate, string Descrption, string Imgname)
    {
        string DefaultImage = "d.jpg";
              
        SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlCommand cmd1 = new SqlCommand("insert_Event", conn1);
        cmd1.CommandType = CommandType.StoredProcedure;
        cmd1.Parameters.AddWithValue("@EventTitle", txtTitle);
        cmd1.Parameters.AddWithValue("@EventTime", txtEventTime);
        cmd1.Parameters.AddWithValue("@EventVenue", txtLocation);
        cmd1.Parameters.AddWithValue("@EventDate",Convert.ToDateTime(txtDate));
        cmd1.Parameters.AddWithValue("@DESCRIPTION", Descrption);
        cmd1.Parameters.AddWithValue("@Img", String.IsNullOrEmpty(Imgname)?DefaultImage:Imgname);

        conn1.Open();
        cmd1.ExecuteNonQuery();
        conn1.Close();

    }    
}